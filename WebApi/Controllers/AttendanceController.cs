﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http;

namespace WebApi.Controllers
{
    [RoutePrefix("api/attendance")]
    public class AttendanceController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        //[HttpGet]
        //[Route("addAttendanceList")]
        //public IHttpActionResult AddAttendanceList(List<Attendance> listAttendance)
        //{
        //    using (TpsDbEntities db = new TpsDbEntities())
        //    {
        //        AttendanceService attendanceService = new AttendanceService(db);
        //        try
        //        {
        //            foreach (var attendance in listAttendance)
        //            {
        //                attendanceService.CreateAttendance(attendance);
        //            }
        //            return Json(new
        //            {
        //                success = true,
        //                message = "Thêm thành công"
        //            });
        //        }
        //        catch (Exception e) {
        //            return Json(new
        //            {
        //                success = false,
        //                message = "Có lỗi xảy ra"
        //            });
        //        }
        //    }
        //}
        

        //[HttpGet]
        //[Route("all")]
        //public IHttpActionResult GetAllMovie()
        //{
        //    using(TpsDbEntities db = new TpsDbEntities())
        //    {
        //        AccountService accountService = new AccountService(db);
        //        IEnumerable<JObject> jResult = accountService
        //            .getAll()
        //            .Select(acc => new JObject
        //            {
        //                ["Username"] = acc.Username,
        //                ["Password"] = acc.Password
        //            });
        //        return Ok(new JArray(jResult));
        //    }
        //}


        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }

}
