﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using HmsService.Models.Entities;
using HmsService.Models.Entities.Services;
using HmsService.Sdk;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;

namespace WebApi.Controllers
{
    [RoutePrefix("api/account")]
    public class ValuesController : ApiController
    {
        private readonly Cloudinary _cloudinary;
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        //[HttpGet]
        //[Route("CheckLogin")]
        //public IHttpActionResult CheckLogin(String username, String password)
        //{
        //    AccountApi accountApi = new AccountApi();
        //    List<HmsService.Models.Entities.Account> result = accountApi.GetAllAccount().Where(acc => acc.Username == username && acc.Password == password).ToList();
        //    if (result != null)
        //    {
        //        StoreModel storeModel = result.Select(acc => new StoreModel
        //        {
        //            Id = acc.Store.Id,
        //            Name = acc.Store.Name,
        //        }).FirstOrDefault();
        //        String storeJson = JsonConvert.SerializeObject(storeModel);
        //        return Ok(storeJson);
        //    }
        //    else return NotFound();
        //}
        //[Route("imageUpload/{userId:int}")]
        //[HttpPut]
        //[Authorize]
        //public async Task<IHttpActionResult> AddProfileImage(int userId)
        //{
        //    var httpRequest = HttpContext.Current.Request;
        //    foreach (string file in httpRequest.Files)
        //    {
        //        var postedFile = httpRequest.Files[file];
        //        if (postedFile != null && postedFile.ContentLength > 0)
        //        {
        //            var result = new CloudinaryService().UploadImage(postedFile);
        //            var user = await _uow.Repository<User>().GetAsync(userId);
        //            user.profile.Picture = result.PublicId;
        //            await _uow.SaveChangesAsync();
        //            return Ok(result);
        //        }
        //        return badRequest();
        //    }
        //    return NotFound();
        //}



        //[HttpGet]
        //[Route("GetEmployeeList")]
        //public IHttpActionResult GetEmployeeList(int storeId)
        //{
        //    AccountApi accountApi = new AccountApi();
        //    IEnumerable<HmsService.Models.Entities.Account> result = accountApi.GetAllAccount().Where(acc => acc.RoleId == 1);
        //    if (result != null)
        //    {
        //        List<AccountModel> listAccountModel = result.Select(acc => new AccountModel
        //        {
        //            Username = acc.Username,
        //            Password = acc.Password,
        //            FullName = acc.FullName
        //        }).ToList();
        //        String storeJson = JsonConvert.SerializeObject(listAccountModel);
        //        return Ok(storeJson);
        //    }
        //    else return NotFound();
        //}
        public class StoreModel
        {
            public int? Id { get; set; }
            public string Name { get; set; }
        }
        public class AccountModel
        {
            public string Username { get; set; }
            public string Password { get; set; }
            public string FullName { get; set; }
        }
    }
}
