﻿using HmsService.Models.Entities;
using HmsService.Sdk;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebAdmin.Controllers
{
    public class EmployeeController : Controller
    {
        HmsEntities entities = new HmsEntities();
        // GET: Employee
        public ActionResult Index()
        {
            return View("Editor");
        }

        public ActionResult Create(EmployeeViewModel emp) {
            try
            {
                EmployeeApi employeeApi = new EmployeeApi();
               employeeApi.Create(emp);
            }
            catch (Exception e) {
            }
            return View("Index");
        }
    }
}