﻿using HmsService.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Admin_Capstone_KPS.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //ShiftApi api = new ShiftApi();
            //var a = api.GetActive();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}