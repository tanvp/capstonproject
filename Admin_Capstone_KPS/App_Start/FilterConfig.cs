﻿using System.Web;
using System.Web.Mvc;

namespace Admin_Capstone_KPS
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
