﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Admin_Capstone_KPS.Startup))]
namespace Admin_Capstone_KPS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
