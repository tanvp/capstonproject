﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public class AttendanceService
    {
        private TpsDbEntities tpsEntity;
        public AttendanceService(TpsDbEntities tpsEntity)
        {
            this.tpsEntity = tpsEntity;
        }

        public IEnumerable<Attendance> getAll()
        {
            return tpsEntity.Attendances;
        }
        public Boolean CreateAttendance(Attendance attendace) {
            try {
                tpsEntity.Attendances.Add(attendace);
                tpsEntity.SaveChanges();
                return true;
            }
            catch (Exception e) {
                throw e;
            }
        }
    }
}
