﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public class AccountService
    {
        private TpsDbEntities tpsEntity;
        public AccountService(TpsDbEntities tpsEntity)
        {
            this.tpsEntity = tpsEntity;
        }

        public IEnumerable<Account> getAll()
        {
            return tpsEntity.Accounts;
        }

    }
}
