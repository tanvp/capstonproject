﻿using AutoMapper.QueryableExtensions;
using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Sdk
{
    public partial class AccountApi
    {
        public IEnumerable<Account> GetAllAccount()
        {
            return this.BaseService.GetAllAccount();
        }
    }
}
