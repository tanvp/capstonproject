﻿using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace HmsService.Sdk
{
    public partial class BlogPostApi
    {
        public BlogPost GetBlogPostById(int id)
        {
            var blog = this.BaseService.FirstOrDefault(q => q.Id == id);
            return blog;
        }
        public BlogPost GetBlogPostActiveById(int id)
        {
            var blog = this.BaseService.FirstOrDefault(q => q.Id == id && q.Active);
            return blog;
        }
        public async Task<BlogPostViewModel> GetBlogPostBySeoNameAsync(string seoName, int storeId)
        {
            var entity = await this.BaseService.GetActive(q => q.SeoName == seoName && q.StoreId == storeId)
               .FirstOrDefaultAsync();
            var blogPost = new BlogPostViewModel(entity);
            return blogPost;
        }
        public  BlogPostViewModel GetBlogPostBySeoName(string seoName, int storeId)
        {
            var entity = this.BaseService.GetActive(q => q.SeoName == seoName && q.StoreId == storeId)
               .FirstOrDefault();
            var blogPost = new BlogPostViewModel(entity);
            return blogPost;
        }

        public IQueryable<BlogPost> GetAllActiveByBlogCategoryAndPattern(int categoryId, string pattern)
        {
            var blogPosts = this.BaseService.GetByBlogPostCategoryAndPattern(categoryId, pattern);
            return blogPosts;
        }
        public async Task<BlogPost> GetActiveByStoreAsync(int id, int storeId)
        {
            return await this.BaseService.FirstOrDefaultAsync(q => q.Id == id && q.StoreId == storeId);
        }
        public async Task<BlogPostDetailsViewModel> GetDetailsByStoreIdAsync(int id, int storeId)
        {
            var entity = await this.BaseService.GetActiveDetailsByStoreAsync(id, storeId);

            if (entity == null)
            {
                return null;
            }
            else
            {
                return new BlogPostDetailsViewModel(entity);
            }
        }
        public async Task<BlogPostViewModel> GetByStoreIdAsync(int id, int storeId)
        {
            var entity = await this.BaseService.GetActiveByStoreAsync(id, storeId);

            if (entity == null)
            {
                return null;
            }
            else
            {
                return new BlogPostViewModel(entity);
            }
        }
        public List<BlogPost> GetTopBlogDisplayByCate(int categoryId, int top)
        {
            int status = (int)BlogStatusEnum.Approve;
            var allBlog = this.BaseService.Get(q => (q.BlogCategoryId == categoryId || q.BlogCategory.ParentCateId == categoryId) && q.Status == status)
                .OrderByDescending(q => q.Position).ThenByDescending(q => q.CreatedTime);
            var rs = allBlog.Take(top).ToList();
            return rs;
        }
        public List<BlogPost> GetAllBlogDisplayByCate(int categoryId)
        {
            int status = (int)BlogStatusEnum.Approve;
            var allBlog = this.BaseService.Get(q => (q.BlogCategoryId == categoryId || q.BlogCategory.ParentCateId == categoryId) && q.Status == status)
                .OrderByDescending(q => q.Position).ThenByDescending(q => q.CreatedTime);
            var rs = allBlog.ToList();
            return rs;
        }
        public List<BlogPost> GetBlogEvent(bool isHappened, bool isHappening, bool isComming, int locationID, int skip, int take)
        {
            int status = (int)BlogStatusEnum.Approve;
            int blogType = (int)BlogTypeEnum.Event;
            List<BlogPost> blogHappened = new List<BlogPost>();
            List<BlogPost> blogHappening = new List<BlogPost>();
            List<BlogPost> blogComming = new List<BlogPost>();
            var today = Utils.GetCurrentDateTime();
            List<BlogPost> data = this.BaseService.Get(q => q.Status == status && (locationID == 0 || q.LocationId == locationID) && q.BlogType == blogType).ToList();
            if (isHappened)
            {
                blogHappened = data.Where(q => q.EndDate < today).OrderBy(q => q.StartDate).ToList();
            }
            if (isHappening)
            {
                blogHappening = data.Where(q => q.StartDate <= today && q.EndDate >= today).OrderBy(q => q.StartDate).ToList();
            }
            if (isComming)
            {
                blogComming = data.Where(q => q.StartDate > today).OrderBy(q => q.StartDate).ToList();
            }
            //var allBlogEvent = blogHappened.Concat(blogHappening).Concat(blogComming);
            List<BlogPost> allBlogEvent = new List<BlogPost>();
            allBlogEvent.AddRange(blogHappened);
            allBlogEvent.AddRange(blogHappening);
            allBlogEvent.AddRange(blogComming);
            allBlogEvent.Skip(skip).Take(take);
            return allBlogEvent;
        }

        public List<BlogPost> GetTopViewBlogDisplay(int top)
        {
            int status = (int)BlogStatusEnum.Approve;
            var allBlog = this.BaseService.Get(q => q.Status == status && q.BlogType == (int)BlogTypeEnum.News).OrderByDescending(q => q.TotalVisit).Take(top);
            return allBlog.ToList();
        }
        public List<BlogPost> GetTopViewBlogDisplayInCate(int cateId, int top)
        {
            int status = (int)BlogStatusEnum.Approve;
            var allBlog = this.BaseService.Get(q => q.Status == status && q.BlogCategoryId == cateId).OrderByDescending(q => q.TotalVisit).Take(top);
            return allBlog.ToList();
        }
        public IQueryable<BlogPost> GetBlogPostByBlogCateId(int categoryId)
        {
            int status = (int)BlogStatusEnum.Approve;
            var allBlog = this.BaseService.Get(q => q.BlogCategoryId == categoryId && q.Status == status)
                .OrderByDescending(q => q.Position).ThenByDescending(q => q.CreatedTime);
            return allBlog;
        }
        public List<BlogPost> GetContinuteBlogDisplayByCate(int categoryId, int skip, int take)
        {
            int status = (int)BlogStatusEnum.Approve;
            var allBlog = this.BaseService.Get(q => (q.BlogCategoryId == categoryId || q.BlogCategory.ParentCateId == categoryId) && q.Status == status)
                .OrderByDescending(q => q.Position).ThenByDescending(q => q.CreatedTime);
            var rs = allBlog.Skip(skip).Take(take).ToList();
            return rs;
        }

        public BlogPost GetDetailBlog(int blogId)
        {
            return this.BaseService.Get(blogId);
        }
        /// <summary>
        /// Get Id Blog by SEO
        /// </summary>
        /// <param name="seoName"></param>
        /// <returns> Value Id of Blog; return -1 When Not Found;</returns>
        public int GetIdPostBySeoName(string seoName, int storeId)
        {
            var blog = this.BaseService.FirstOrDefault(q => q.SeoName == seoName && storeId == q.StoreId);
            if (blog != null)
            {
                return blog.Id;
            }
            else
            {
                return -1;
            }
        }

        public List<BlogPost> GetTopBlogRefCate(int blogId, int numberTake)
        {
            var blog = this.BaseService.Get(blogId);
            var result = this.BaseService.Get(q => q.BlogCategoryId == blog.BlogCategoryId).OrderByDescending(q => q.UpdatedTime).Take(numberTake).ToList();
            return result;
        }

        public List<BlogPost> GetHotNew(int numberTake)
        {
            var result = this.BaseService.Get().Where(q=>q.Active && q.BlogCategory.IsActive == true && q.BlogCategory.IsDisplay == true && q.Status == (int)BlogStatusEnum.Approve).OrderByDescending(b => b.UpdatedTime).Take(numberTake).ToList();
            return result;
        }
        public List<BlogPost> GetHotNew(string seoName, int numberTake)
        {
            var result = this.BaseService.Get().Where(q => q.Active && q.BlogCategory.IsActive == true
                                                        && q.BlogCategory.IsDisplay == true 
                                                        && q.Status == (int)BlogStatusEnum.Approve
                                                        && q.BlogPostCollectionItemMappings.FirstOrDefault().BlogPostCollection.SeoName.Equals(seoName))
                                                        .OrderByDescending(b => b.UpdatedTime).Take(numberTake).ToList();
            return result;
        }

        public int GetIdPostBySeoNameAndType(string seoName, int storeId, int type)
        {
            var blog = this.BaseService.FirstOrDefault(q => q.SeoName == seoName && storeId == q.StoreId && type == q.BlogType);
            if (blog != null)
            {
                return blog.Id;
            }
            else
            {
                return -1;
            }
        }
        public IEnumerable<BlogPost> GetAllBlogPostByType(int type, int brandId)
        {
            var blog = this.BaseService.Get(q => q.BlogType == type && q.BrandId == brandId && q.Active && q.BlogCategory.IsActive == true && q.BlogCategory.IsDisplay == true);
            return blog;
        }
        public List<ReferenceLink> GetLinkReference(int blogId)
        {
            var listResult = new List<ReferenceLink>();
            var blog = this.BaseService.Get(blogId);
            if (blog != null)
            {
                if (!string.IsNullOrWhiteSpace(blog.LinkRef1))
                {
                    var seoName = blog.LinkRef1.Split('/').Last();
                    if (!string.IsNullOrWhiteSpace(seoName))
                    {
                        var tempRef = new ReferenceLink();
                        var blogtmp = this.BaseService.GetBlogPostBySeoName(seoName);
                        if (blogtmp != null)
                        {
                            tempRef.linkAccess = Utils.GetFullPathBlogBaseOnSEOName(blogtmp);
                            tempRef.tile = blogtmp.Title;
                            listResult.Add(tempRef);
                        }
                    }

                }
                if (!string.IsNullOrWhiteSpace(blog.LinkRef2))
                {
                    var seoName = blog.LinkRef2.Split('/').Last();
                    if (!string.IsNullOrWhiteSpace(seoName))
                    {
                        var tempRef = new ReferenceLink();
                        var blogtmp = this.BaseService.GetBlogPostBySeoName(seoName);
                        if (blogtmp != null)
                        {
                            tempRef.linkAccess = Utils.GetFullPathBlogBaseOnSEOName(blogtmp);
                            tempRef.tile = blogtmp.Title;
                            listResult.Add(tempRef);
                        }
                    }

                }
                if (!string.IsNullOrWhiteSpace(blog.LinkRef3))
                {
                    var seoName = blog.LinkRef3.Split('/').Last();
                    if (!string.IsNullOrWhiteSpace(seoName))
                    {
                        var tempRef = new ReferenceLink();
                        var blogtmp = this.BaseService.GetBlogPostBySeoName(seoName);
                        if (blogtmp != null)
                        {
                            tempRef.linkAccess = Utils.GetFullPathBlogBaseOnSEOName(blogtmp);
                            tempRef.tile = blogtmp.Title;
                            listResult.Add(tempRef);
                        }
                    }

                }
                if (!string.IsNullOrWhiteSpace(blog.LinkRef4))
                {
                    var seoName = blog.LinkRef4.Split('/').Last();
                    if (!string.IsNullOrWhiteSpace(seoName))
                    {
                        var tempRef = new ReferenceLink();
                        var blogtmp = this.BaseService.GetBlogPostBySeoName(seoName);
                        if (blogtmp != null)
                        {
                            tempRef.linkAccess = Utils.GetFullPathBlogBaseOnSEOName(blogtmp);
                            tempRef.tile = blogtmp.Title;
                            listResult.Add(tempRef);
                        }
                    }

                }
                if (!string.IsNullOrWhiteSpace(blog.LinkRef5))
                {
                    var seoName = blog.LinkRef5.Split('/').Last();
                    if (!string.IsNullOrWhiteSpace(seoName))
                    {
                        var tempRef = new ReferenceLink();
                        var blogtmp = this.BaseService.GetBlogPostBySeoName(seoName);
                        if (blogtmp != null)
                        {
                            tempRef.linkAccess = Utils.GetFullPathBlogBaseOnSEOName(blogtmp);
                            tempRef.tile = blogtmp.Title;
                            listResult.Add(tempRef);
                        }
                    }

                }
            }
            return listResult;
        }

        public async Task CreateAsync(BlogPostViewModel model, int[] blogPostCollectionIds, string[] images, int[] tags)
        {
            model = Utils.ToExactType<BlogPostViewModel, BlogPostViewModel>(model);

            var entity = model.ToEntity();
            entity.Image = images.FirstOrDefault();

            await this.BaseService.CreateAsync(entity, blogPostCollectionIds, images, tags);
        }

        public async Task EditAsync(BlogPostViewModel model, int[] blogPostCollectionIds, string[] images, int[] tags)
        {
            model = Utils.ToExactType<BlogPostViewModel, BlogPostViewModel>(model);

            var entity = await this.BaseService.GetAsync(model.Id);
            model.CopyToEntity(entity);
            entity.Image = images.FirstOrDefault();

            await this.BaseService.UpdateAsync(entity, blogPostCollectionIds, images, tags);
        }
        public IEnumerable<BlogPost> GetHotBlogWithSixItems(int categoryId, int storeId)
        {
            var res = this.BaseService.Get(q => q.Active && q.StoreId == storeId && q.IsHotNews && q.BlogCategoryId == categoryId);
            if (res.Count() >= 6)
            {
                res = res.Take(6);
            }
            return res.AsEnumerable();
        }

    }
}
