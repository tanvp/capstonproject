﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using HmsService.Models.Entities;
using HmsService.ViewModels;

namespace HmsService.Sdk
{
    public partial class AdsRegisterApi
    {
        public List<AdsRegisterViewModel> GetAllAdsRegister()
        {
            var result = this.BaseService.GetAllAdsRegister()
                .ProjectTo<AdsRegisterViewModel>(this.AutoMapperConfig)
                .ToList();
            return result;
        }
        public List<AdsRegisterViewModel> GetAllActiveValidAdsRegister()
        {
            var result = this.BaseService.GetAllActiveValidAdsRegister().OrderByDescending(q => q.Position)
                .ProjectTo<AdsRegisterViewModel>(this.AutoMapperConfig)
                .ToList();
            return result;
        }
        /// <summary>
        /// Get ads that endtime >= today
        /// </summary>
        /// <returns></returns>
        public IQueryable<AdsRegister> GetAllAdsRegisterNotEnd()
        {
            return this.BaseService.Get(q => q.EndDate >= System.DateTime.Today);
        }

        public AdsRegister GetAdsRegisterById(int adsId)
        {
            return this.BaseService.Get(p => p.Id == adsId).FirstOrDefault();
        }

        public IQueryable<string> GetAllCompanies(int brandId)
        {
            return this.BaseService.Get(q => q.BrandID == brandId).Select(c => c.Company);
        }
    }
}
