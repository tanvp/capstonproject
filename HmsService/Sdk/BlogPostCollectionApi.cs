﻿using AutoMapper.QueryableExtensions;
using HmsService.Models;
using HmsService.Models.Entities;
using HmsService.ViewModels;
using SkyWeb.DatVM.Mvc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Sdk
{

    public partial class BlogPostCollectionApi
    {
        //Async method
        public async Task<BlogPostCollectionWithPostsViewModel> GetDetailsAsync(int id)
        {
            var entity = await this.BaseService.GetAsync(id);

            if (entity == null) { return null; }

            var result = new BlogPostCollectionWithPostsViewModel(entity);
            result.BlogPostCollectionItems = entity.BlogPostCollectionItemMappings
                .AsQueryable()
                .Where(a => a.Active && a.BlogPost.Active)
                .OrderBy(a => a.Id)
                .ProjectTo<BlogPostCollectionItemWithBlogPostViewModel>(this.AutoMapperConfig);
            return result;
        }

        public PagingViewModel<BlogPostCollectionDetailsViewModel> GetAdminWithFilterAsync(int brandId, string keyword, int currPage, int pageSize, KeyValuePair<string, bool> sortKeyAsc)
        {
            var pagedList = this.BaseService.GetAdminByStoreWithFilter(brandId, keyword, sortKeyAsc)
                .ProjectTo<BlogPostCollectionDetailsViewModel>(this.AutoMapperConfig)
                .Page(currPage, pageSize);

            return new PagingViewModel<BlogPostCollectionDetailsViewModel>(pagedList);
        }

        public List<BlogPost> GetBlogPostByCollection(int brandId, int collectionID)
        {
            var blogPostCollectionMapping = new BlogPostCollectionItemMappingApi();
            //Get 5 news 
            return blogPostCollectionMapping.BaseService.Get(q => q.BlogPostCollectionId == collectionID && q.Active == true).Select(q => q.BlogPost).Where(q => q.BlogType == (int)BlogTypeEnum.News && q.Status == (int)BlogStatusEnum.Approve).Take(5).ToList();
        }


        public List<BlogPost> GetAllBlogPostByCollection(int brandId, int collectionID)
        {
            var blogPostCollectionMapping = new BlogPostCollectionItemMappingApi();
            return blogPostCollectionMapping.BaseService.Get(q => q.BlogPostCollectionId == collectionID && q.Active == true).Select(q => q.BlogPost).Where(q => q.BlogType == (int)BlogTypeEnum.News && q.Status == (int)BlogStatusEnum.Approve).ToList();
        }

        public async Task<IEnumerable<BlogPostCollectionViewModel>> GetActiveByStoreIdAsync(int brandId)
        {
            return await this.BaseService.GetActiveByBrand(brandId)
                .ProjectTo<BlogPostCollectionViewModel>(this.AutoMapperConfig)
                .ToListAsync();
        }


        //Sync method

        public BlogPostCollectionWithPostsViewModel GetDetails(int id)
        {
            var entity = this.BaseService.Get(id);

            if (entity == null) { return null; }

            var result = new BlogPostCollectionWithPostsViewModel(entity);
            result.BlogPostCollectionItems = entity.BlogPostCollectionItemMappings
                .AsQueryable()
                .Where(a => a.Active && a.BlogPost.Active)
                .OrderBy(a => a.Id)
                .ProjectTo<BlogPostCollectionItemWithBlogPostViewModel>(this.AutoMapperConfig);
            return result;
        }

        public IEnumerable<BlogPostCollection> GetActiveByBrandId(int brandId)
        {
            return this.BaseService.GetActiveByBrand(brandId);
        }
        public int DetectPathCollection(string seoName, int storeId)
        {
            var storeApi = new StoreApi();
            var brandId = storeApi.BaseService.Get(storeId).BrandId;
            var collection = this.BaseService.FirstOrDefault(q => q.SeoName == seoName && q.BrandId == brandId);
            if (collection != null)
            {
                return collection.Id;
            }
            else
            {
                return -1;
            }
        }
        public int GetIdCollectionBySeoName(string seoName, int brandId)
        {
            var collection = this.BaseService.FirstOrDefault(q => q.SeoName == seoName && q.BrandId == brandId);
            if (collection != null)
            {
                return collection.Id;
            }
            else
            {
                return -1;
            }
        }
        public List<BlogPost> GetTopBlogDisplayByCollection(int collectionId, int top)
        {
            //int status = (int)BlogStatusEnum.Approve;
            var allBlog = this.BaseService.Get(q => (q.Id == collectionId ||q.ParentId==collectionId) && q.Active && q.IsDisplay)
                .FirstOrDefault();
            if (allBlog != null)
            {
                return allBlog.BlogPostCollectionItemMappings.Select(q => q.BlogPost)
                .OrderByDescending(q => q.Position).ThenByDescending(q => q.CreatedTime).Take(top).ToList();
            }

            return null;
        }
        public List<BlogPost> GetContinuteBlogDisplayByCollection(int collectionId, int skip, int take)
        {
            var rs = new List<BlogPost>();
            var allBlog = this.BaseService.Get(q => (q.Id == collectionId || q.ParentId == collectionId) && q.Active && q.IsDisplay)
                .FirstOrDefault();
            if (allBlog != null)
            {
                rs = allBlog.BlogPostCollectionItemMappings.Select(q => q.BlogPost)
                .OrderByDescending(q => q.Position).ThenByDescending(q => q.CreatedTime).Skip(skip).Take(take).ToList();
            }

            return rs;
        }
    }

}
