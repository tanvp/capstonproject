﻿using HmsService.Models.Entities.Services;
using HmsService.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using HmsService.Models.Entities;

namespace HmsService.Sdk
{
    public partial class BlogCategoryApi
    {
        public IQueryable<BlogCategoryViewModel> GetAllBlogCategoryActiveByBrandId(int brandId)
        {
            return this.BaseService.GetAllBlogCategoryActiveByBrandId(brandId).ProjectTo<BlogCategoryViewModel>(this.AutoMapperConfig);
        }
        public IQueryable<BlogCategoryViewModel> GetAllParentBlogCategoryActiveByBrandId(int brandId)
        {
            return this.BaseService.GetAllParentBlogCategoryActiveByBrandId(brandId).ProjectTo<BlogCategoryViewModel>(this.AutoMapperConfig);
        }

        public IQueryable<BlogCategory> GetAllBlogCategoryActive()
        {
            return this.BaseService.Get(q => q.IsActive == true);
        }

        public IQueryable<BlogCategoryViewModel> GetAllChildCate(int blogCate)
        {
            return this.BaseService.Get(q => q.ParentCateId == blogCate && q.IsActive && q.IsDisplay == true).ProjectTo<BlogCategoryViewModel>(this.AutoMapperConfig);
        }

        public IQueryable<BlogCategory> GetParrentCategory(int brandId)
        {
            return this.BaseService.Get(q => q.BrandId == brandId && q.ParentCateId == null && q.IsActive == true && q.IsDisplay == true);
        }

        public BlogCategory GetBlogCategoryById(int cateId)
        {
            return this.BaseService.Get(p => p.Id == cateId).FirstOrDefault();
        }

        public int GetIdCateBySeoName(string seoName, int brandId)
        {
            var cate = this.BaseService.FirstOrDefault(q => q.SeoName == seoName && q.BrandId == brandId);
            if (cate != null)
            {
                return cate.Id;
            }
            else
            {
                return -1;
            }
        }
        public List<int> GetIdCateByType(int type, int brandId)
        {
            var cate = this.BaseService.Get(q => q.Type == type && q.BrandId == brandId && q.IsDisplay == true).OrderBy(q => q.Position).Select(q => q.Id).ToList();
            return cate;
        }
        public List<int> GetIdCateParentByType(int type, int brandId)
        {
            var cate = this.BaseService.Get(q => q.Type == type && q.BrandId == brandId && q.IsDisplay == true && q.ParentCateId == null).OrderBy(q => q.Position).Select(q => q.Id).ToList();
            return cate;
        }
        public IQueryable<BlogCategory> GetListCateByType(int type, int brandId)
        {
            var listCate = this.BaseService.Get(q => q.Type == type && q.BrandId == brandId && q.IsActive == true);
            return listCate;
        }

        public int DetectPath(string seoName, int storeId, int type)
        {
            var storeApi = new StoreApi();
            var brandId = storeApi.BaseService.Get(storeId).BrandId;
            var cate = this.BaseService.FirstOrDefault(q => q.SeoName == seoName && q.Type == type && q.BrandId == brandId);
            if (cate != null)
            {
                return cate.Id;
            }
            else
            {
                return -1;
            }
        }
        

    }
}
