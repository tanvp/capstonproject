﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Models.Entities.Services
{
    public partial interface IBlogCategoryService
    {
        IQueryable<BlogCategory> GetAllBlogCategoryActiveByBrandId(int brandId);
        IQueryable<BlogCategory> GetAllParentBlogCategoryActiveByBrandId(int brandId);
    }
    public partial class BlogCategoryService
    {
        public IQueryable<BlogCategory> GetAllBlogCategoryActiveByBrandId(int brandId)
        {
            return this.Get(q => q.IsActive && q.BrandId == brandId && q.IsDisplay== true);
        }
        public IQueryable<BlogCategory> GetAllParentBlogCategoryActiveByBrandId(int brandId)
        {
            return this.Get(q => q.IsActive && q.BrandId == brandId && q.IsDisplay == true && q.ParentCateId==null);
        }
    }
}
