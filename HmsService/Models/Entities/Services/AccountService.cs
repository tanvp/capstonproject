﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Models.Entities.Services
{
    public partial interface IAccountService
    {

        IEnumerable<Account> GetAllAccount();

    }

    public partial class AccountService
    {
        public IEnumerable<Account> GetAllAccount()
        {
            IEnumerable<Account> employeeList = this.Get().ToList();
            return employeeList;
        }
    }
}
