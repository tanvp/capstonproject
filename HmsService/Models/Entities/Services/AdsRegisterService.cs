﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Models.Entities.Services
{
    public partial interface IAdsRegisterService
    {
        IQueryable<AdsRegister> GetAllAdsRegister();
        IQueryable<AdsRegister> GetAllActiveValidAdsRegister();
    }
    public partial class AdsRegisterService
    {
        public IQueryable<AdsRegister> GetAllAdsRegister()
        {
            return this.Get();
        }
        public IQueryable<AdsRegister> GetAllActiveValidAdsRegister()
        {
            var now = Utils.GetCurrentDateTime();
            return this.Get(q => q.isActive.Value && q.StartDate.Value <= now && q.EndDate.Value >= now);
        }
    }
}
