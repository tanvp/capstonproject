﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.Models.Entities.Services
{
    public partial interface IEmployeeService
    {

        IEnumerable<Employee> GetAllEmployee();

    }

    public partial class EmployeeService
    {
        public IEnumerable<Employee> GetAllEmployee()
        {
            IEnumerable<Employee> employeeList = this.Get().ToList();
            return this.Get(q => q.EmployeeId == 1);
        }
    }
}
