//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HmsService.Models.Entities.Services
{
    using System;
    using System.Collections.Generic;
    
    
    public partial interface ITimeModeService : SkyWeb.DatVM.Data.IBaseService<TimeMode>
    {
    }
    
    public partial class TimeModeService : SkyWeb.DatVM.Data.BaseService<TimeMode>, ITimeModeService
    {
        public TimeModeService(SkyWeb.DatVM.Data.IUnitOfWork unitOfWork, Repositories.ITimeModeRepository repository) : base(unitOfWork, repository)
        {
        }
    }
}
