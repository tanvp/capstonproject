﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HmsService.ViewModels
{
    public partial class AdsRegisterViewModel : SkyWeb.DatVM.Mvc.BaseEntityViewModel<HmsService.Models.Entities.AdsRegister>
    {

        public virtual string stringDateStart { get;set; }
        public virtual string stringDateEnd { get; set; }

    }
}
